$('.delete1').on("click", function() {
    $('.first-ap').remove();
});

$('.delete2').on("click", function() {
    $('.second-ap').remove();
});

$('.delete3').on("click", function() {
    $('.third-ap').remove();
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
  })


$('#pills-second').on("click", function () {
    $('#pills-second').attr('id', 'pills-active');
    $('#pills-active').attr("id", 'pills-second');
    $('#pills-second').addClass('bl-grey');
    $('#pills-active').addClass('changed');
});

$('#pills-active').on("click", function () {
    $('#pills-active').attr("id", "pills-second");
    $('#pills-second').attr("id", "pills-active");
    $('#pills-second').removeClass('bl-grey');
});

$('.more-packages').on("click", function(){
    $('.additional-packages').toggle();
    $('.more-packages').toggle();
    $('.less-packages').toggle();
});

$('.less-packages').on("click", function(){
    $('.additional-packages').toggle();
    $('.more-packages').toggle();
    $('.less-packages').toggle();
});

$('.read-more-invoke').on("click", function() {
    $('.read-more').toggle();
    $('.read-more-invoke').attr("style", "display: none !important");
});

$('.modal')
    .on('show.bs.modal', function (){
            $('body').css('overflow', 'hidden');
        })
    .on('hide.bs.modal', function (){
            // Also if you are using multiple modals (cascade) - additional check
                $('body').css('overflow-y', 'auto');
                $('body').css('overflow-x', 'hidden');

    });

// Get the header
var header = document.getElementById("topbar");
var map = document.getElementById("map");

if(header != null) {
    var sticky = header.offsetTop;
    function myFunction() {
        if (window.pageYOffset > sticky && window.pageYOffset < 1850) {
            console.log(window.pageYOffset);
            header.classList.add("sticky");
            if($(window).width() > 1200) {
                map.classList.add("sticky-map");
                map.classList.remove("absolute-map");
            }
            
        } else if(window.pageYOffset > 1851) {
            if($(window).width() > 1200) {
                map.classList.add("absolute-map");
            }
            map.classList.remove("sticky-map");
        } else {
            header.classList.remove("sticky");
            map.classList.remove("sticky-map");
        }
    }
    window.onscroll = function() {myFunction()};
}



$(".arrow-change").click(function() {
    $(this).find('img').toggle();
    $(this).find('i').toggle();
});

$(".arrow-change-title").click(function() {
    $(this).next().find('img').toggle();
    $(this).next().find('i').toggle();
});

$('#fullscreen').click(function() {
    $("#map").removeClass("d-none").addClass("d-block d-md-block");
    setTimeout(function() {
        $('div.gm-style button[title="Toggle fullscreen view"]').trigger('click');
    }, 1000)
    
});



$(document).bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function() {
    var isFullScreen = document.fullScreen ||
        document.mozFullScreen ||
        document.webkitIsFullScreen;
    if (isFullScreen) {
        console.log('fullScreen!');
    } else {
        $("#map").removeClass("d-block d-md-block").addClass("d-none d-md-none");
    }
});

$('#show-text').on('click', function() {
    $('.show-text').toggle();
});

$( document ).ready(function() {
    if($('.tab-content .discount__signup').parent().height() > 380) {
        $('.tab-content .discount__signup').addClass('discount-absolute');
    }
    
});



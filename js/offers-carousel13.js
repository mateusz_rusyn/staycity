//==========================================================================
////////////////////////////////   VARIABLES  //////////////////////////////
//==========================================================================


var arrivalSelected = false;
var departureSelected = false;
var nights = 1;

var arrivalDayIndex;
var departureDayIndex;
var nextFullyBookedIndex;

var dateArrivalString = null;
var dateDepartureString = null;

var month = "mar";
var currentSlide = 0;



//==========================================================================
//////////////////////////////   FUNCTIONS  ////////////////////////////////
//==========================================================================


function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}

function returnDateAsString(dateIndex){
	var wholeStringId = "#day"+dateIndex;
	console.log("The id is "+wholeStringId);
	var date = $(wholeStringId).find('.day').html();
	var month = $(wholeStringId).find('.month').html();
	var dateAsString = date+month;
	return dateAsString;
}


function getNights(){
	nights = $("#select-nights :selected").val();
	console.log("Staying "+ nights +" nights");
}


function returnIndexFromDay(thingClicked){
	var wholeStringId = thingClicked.attr('id');
	// console.log(wholeStringId);
	var index = wholeStringId.split("y");
	index = index[index.length-1];
	// console.log(index);
	return index;
}

function findNextFullyBooked (){
	$('.date-box').each(function(){
			//check its index
		var eachIndex = returnIndexFromDay($(this));
			if( eachIndex > arrivalDayIndex && $(this).hasClass("departure-only")){
				nextFullyBookedIndex = eachIndex;
				console.log("The next fully booked date index is "+nextFullyBookedIndex);
				return false;
			}
	});// end of Each
}

function removeCurrentArrival(){
	//get the current arrival id
	var currentArrivalId = "day"+ arrivalDayIndex;
	//remove the styling from the current arrival date
	$("#"+currentArrivalId).removeClass( "date-selected" );
	arrivalDayIndex = null;
	departureDayIndex = null;
	arrivalSelected = false;
	departureSelected = false;
	dateArrivalString = null;
	nextFullyBookedIndex = null;
	removeDatesBetweenStyling();
}

function setNewDeparture(){
	departureSelected = true;
	getNights();
	// console.log("The arrival day index is " + arrivalDayIndex);
	departureDayIndex = +arrivalDayIndex + +nights;
	//add a zero in front if it's single digits, so that it can be passed into the function to create the id.
	if (departureDayIndex<=9){
		departureDayIndex = "0"+departureDayIndex;
	}else{
		departureDayIndex = departureDayIndex;
	}
	dateDepartureString = returnDateAsString(departureDayIndex);
	console.log("The departure Date is "+dateDepartureString);
	$(".departing-message").html("Leave on "+dateDepartureString);
	// console.log("The departure day index is "+departureDayIndex);
	// dateDepartureString = returnDateAsString(thingClicked);
}

function highlightDaysBetween(){
	$('.date-box').each(function(){
		var indexOfDate = returnIndexFromDay($(this));

		if(indexOfDate>arrivalDayIndex && indexOfDate<departureDayIndex){
			$(this).addClass("dateBetween");
		}else{
			$(this).removeClass("dateBetween");
			// console.log("dateBetween class was removed somewhere");
		}	//end of if else
	});	//end of each
}	//end of highlightDatesBetween

function highlightDaysAfterUnavailable(thingClicked){
	var unavailableDayIndex = returnIndexFromDay(thingClicked);
	var wouldBeDepartureDateIndex = (+unavailableDayIndex + +nights);
	console.log("Departure date would have been "+wouldBeDepartureDateIndex);
	// for each of the days that has an index greater than unavailableDayIndex, and less than (unavailableDayIndex plus no. nights)
	// add the styling
	$('.date-box').each(function(){
		var indexOfDate = returnIndexFromDay($(this));
		if(indexOfDate>unavailableDayIndex && indexOfDate<wouldBeDepartureDateIndex){
			$(this).addClass("dateBetween");
		}else{
			$(this).removeClass("dateBetween");
		}
		
	});	//end of each

}


function highlightFullyBooked(problemDate){
	problemDate.addClass("problem-date");
}

function resetHighlightFullyBooked(){
	$(".problem-date").removeClass("problem-date"); 
}

function setNewDates(thingClicked){

	if(thingClicked.hasClass("departure-only")){
		console.log("You can't arrive on that date!");
		$(".book-btn").animate({width: '0%'});
		$(".unavailable-message").show();
		highlightDaysBetween();
		highlightDaysAfterUnavailable(thingClicked);
		resetHighlightFullyBooked();
		highlightFullyBooked(thingClicked);
	} else {

		thingClicked.addClass( "date-selected" );
		arrivalSelected = true;
		arrivalDayIndex = returnIndexFromDay(thingClicked);
		dateArrivalString = returnDateAsString(arrivalDayIndex);
		console.log("Arrival Date is "+dateArrivalString);
		$(".arriving-message").html("Arrive on "+dateArrivalString);

		findNextFullyBooked();
		setNewDeparture();
		$(".sticky-message").slideDown();
			//if all the days are available
			if(departureDayIndex<=nextFullyBookedIndex){
				highlightDaysBetween();
				resetHighlightFullyBooked();
				// $(".book-btn").slideDown();
				$(".book-btn").animate({width: '50%'});
				$(".unavailable-message").hide();
			} else {
				//if any or all of the days are unavailable
				var nextFullyBooked = $("#day"+nextFullyBookedIndex);
				highlightDaysBetween();
				highlightFullyBooked(nextFullyBooked);
				$(".book-btn").animate({width: '0%'});
				$(".unavailable-message").show();
			}  //end of little if statement
	}//end of if statement
}// end of function setNewDates



function removeDatesBetweenStyling(){
	$('.date-box').each(function(){
		$(this).removeClass("dateBetween");
	});
}

function updateCurrentSlide(){
	currentSlide = $('.slider').slick('slickCurrentSlide');
}






//===========================================================================================================
//===========================================================================================================



$( document ).ready(function() {



//==========================================================================
///////////////////////   WHEN FIND DATES iS CLICKED  //////////////////////
//==========================================================================


//When the FIND DATES button is clicked, and when the calendar open, set up the slick slider


$('.dates-btn').click(function(){

var top = $('.dates-btn').offset().top;

if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {           
            window.scrollTo(0,top) // first value for left offset, second value for top offset
}else{
            $('html,body').animate({
                scrollTop: top,
                scrollLeft: 0
            }, 800, function(){
                $('html,body').clearQueue();
            });
}

 // $('html, body').animate({
 //        scrollTop: $('.dates-btn').offset().top
 //  	}, 600);

})

$('#calendar').on('shown.bs.collapse', function () {
	//Scroll down so the slider starts at the top of the screen
  	$('.slider').slick({dots: false, slidesToShow: 14, arrows: true, vertical: true, infinite: false, slidesToScroll: 10, mobileFirst: true, draggable: true, initialSlide: 0, waitForAnimate: false, adaptiveHeight: true, prevArrow: $('.top-arrow'), nextArrow: $('.bottom-arrow')}
    	); 
})


// On after slide change
$('.slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
	updateCurrentSlide();
    if(currentSlide<1){
    	$('.top-arrow').addClass("disabled");
    }else if(currentSlide>90){
    	$('.bottom-arrow').addClass("disabled");
    }else{
    	$('.top-arrow').removeClass("disabled");
    	$('.bottom-arrow').removeClass("disabled");		
    }
});


//==========================================================================
////////////////////////   WHEN A DATE IS CLICKED  /////////////////////////
//==========================================================================


$(document).on('click', ".date-box", function() {
    var indexJustClicked = returnIndexFromDay($(this));

    //when a date is clicked.
    //if nothing has been selected yet
    if(!arrivalSelected && !departureSelected){
    	removeCurrentArrival();
    	setNewDates($(this));  
    }
    //if both arrival and departure have been set
    else if(arrivalSelected && departureSelected){
    	removeCurrentArrival();
    	setNewDates($(this));
    }else{
    	alert("Something has gone wrong. The arrival date index is "+ arrivalDayIndex +" and the departure day index is "+departureDayIndex+". The arrivalSelected is "+arrivalSelected+"and the departureSelected is "+departureSelected );
    }
});

//==========================================================================
/////////////////////    WHEN NIGHTS IS CHANGED     ////////////////////////
//==========================================================================

$(document).on('change', "#select-nights", function() {
	console.log("------------The number of nights has changed!-------------");
	setNewDeparture();
	if(departureDayIndex<=nextFullyBookedIndex){
				highlightDaysBetween();
				resetHighlightFullyBooked();
				$(".book-btn").animate({width: '50%'});
			} else {
				var nextFullyBooked = $("#day"+nextFullyBookedIndex);
				highlightDaysBetween();
				highlightFullyBooked(nextFullyBooked);
				$(".book-btn").animate({width: '0%'});
				$(".unavailable-message").show();
			}  //end of little if statement
});



//==========================================================================
/////////////////////    WHEN BOOKED IS CLICKED     ////////////////////////
//==========================================================================


$('.book-btn').click(function(){
	var arrivalAsString = returnDateAsString(arrivalDayIndex);
	var departureAsString = returnDateAsString(departureDayIndex);
	alert("The dates you have selected are: \n\nArriving : " + arrivalAsString + ".\nStaying : " +nights+ " nights.\nDeparting : " +departureAsString+ ".");
})

});//end of document.ready function












